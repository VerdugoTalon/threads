from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Thread, Comment
from .forms import ThreadForm, CommentForm
@login_required
def my_core_list(request):
    threads = Thread.objects.filter(user=request.user)
    return render(request, 'core/my_core_list.html', {'threads': threads})
def index(request):
    threads = Thread.objects.all()
    return render(request, 'core/index.html', {'threads': threads})

def thread_detail(request, id):
    thread = get_object_or_404(Thread, id=id)
    comments = Comment.objects.filter(thread=thread)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.thread = thread
            comment.save()
            return redirect('thread_detail', id=thread.id)
    else:
        form = CommentForm()
    return render(request, 'core/thread_detail.html', {'thread': thread, 'comments': comments, 'form': form})

@login_required
def create_thread(request):
    if request.method == 'POST':
        form = ThreadForm(request.POST)
        if form.is_valid():
            thread = form.save(commit=False)
            thread.user = request.user
            thread.save()
            return redirect('index')
    else:
        form = ThreadForm()
    return render(request, 'core/create_thread.html', {'form': form})
